<?php

namespace App\Http\Controllers\sp_controllerapi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use JsonSerializable;

class spController extends Controller
{
    //



  public function insertuser($data)
  {
       $result = DB::select('call spinuser(?,?,?,?)'
                ,[$data['name'],$data['pass'],$data['Idrol'],
                $data['stat']]);

        return $result;
    }


    public function selectuserxid($data)
    {
       $result = DB::select('call spselusexid(?)'
                  ,[$data]);
        return $result;
    }

    public function upuser($data,$id)
    {
      $result = DB::select('call spupduser(?,?,?,?,?)'
                ,[$data['name'],$data['pass'], $data['Idrol'],$data ['stat']
                ,$id]);
      return $result;
    }

    public function Insertrol($data)
    {
      $result = DB::select('call spinrol(?,?)'
                ,[$data['Na'],$data['le']]);
      return $result;
    }

    public function Updaterol($data, $id)
    {

      var_dump($data);
      $result = DB::insert('update  roles  set  name = ?, level = ?
       where id = ?', [$data['Na'],$data['le'], $id]);
      return $result;
    }


}
