<?php

namespace App\Http\Controllers\Admin;

use App\Admin\user;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\sp_controllerapi\spController;
use DB;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $User = DB::select('SELECT * FROM users');
        return view('admin.listuser')->
        with('Users',$User);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $rol = DB::select('SELECT * FROM roles');
      $User = null;
      return view('admin.formuser')->
      with('Drol',$rol)->
      with('DUser',$User);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data = $request->all();
      //
      $a['name']=$request->input("name");
      $a['stat']=  $request->input("stat");
      $a['Idrol']=  $request->input("Idrol");
      $a['pass']= $request->input("pass");

      $resp= (new spController)->insertuser($a);
       if ($resp){

       }else{
         return redirect()->route('C_User.index');
       }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Admin\user  $user
     * @return \Illuminate\Http\Response
     */
    public function show(user $user)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Admin\user  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($user)
    {
      $rol = DB::select('SELECT * FROM roles');
      $User =  (new spController)->selectuserxid($user);
      return view('admin.formuser')->
      with('Drol',$rol)->
      with('DUser',$User);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Admin\user  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user)
    {

        $data= Null;
        $data = $request->all();
        $id = $user;

        $resp= (new spController)->upuser($data, $id);
         if ($resp){
         }else{
           return redirect()->route('C_User.index');
         }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Admin\user  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(user $user)
    {
        //
    }


}
