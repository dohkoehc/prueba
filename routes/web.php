<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    //return view('layout.index');
    return view('login');
});

//Rutas controladores
Route::resource('C_User','Admin\UserController');

Route::get('user', function () {
    // return view('welcome');
    return view('admin.user');
});

Route::post('log', 'Admin\generalcontroller@log');
Route::resource('rol','Admin\RolController');

Route::resource('C_tipserv','Admin\tiposervController');
Route::resource('userespc','Admin\userespcController');
