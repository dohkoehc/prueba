


<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Esfinge</title>
  <!-- Bootstrap core CSS-->
  <link href="{{ asset ('vendor/backend_vedor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{ asset ('vendor/backend_vedor/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
  <!-- Page level plugin CSS-->
  <link href="{{ asset ('vendor/backend_vedor/datatables/dataTables.bootstrap4.css') }}" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{ asset ('css/backend_css/sb-admin.css') }}" rel="stylesheet">
</head>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
{{-- @include('partials.messages') --}}
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">

        <!-- <form> -->

        {{-- {!! Form::open(['url'=>'Administracion/login','method'=>'POST']) !!} --}}
            {{-- @csrf --}}
          <div class="form-group">
            <label for="usuario">Usuario</label>
            <input type="text" class="form-control" name="Usuario">
            </div>
          <div class="form-group">
            <label for="Password">Contraseña:</label>
            <input type="Password" class="form-control" name="Password">
          </div>
        
          <a>

           <button type="submit" method = "post"

          class="btn btn-primary btn-lg btn-block"
          route="postLogin" >Ingresar</button></a>
    {{-- {!! Form::close() !!} --}}
        <!-- </form> -->
        <div class="text-center">
          <a class="d-block small mt-3" href="register.html">Register an Account</a>
          <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
        </div>
      </div>
    </div>
  </div>

    <!--Fin Contenido-->
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small></small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="{{asset ('vendor/backend_vedor/jquery/jquery.min.js') }}"></script>
    <script src="{{asset ('vendor/backend_vedor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{asset ('vendor/backend_vedor/jquery-easing/jquery.easing.min.js') }}"></script>
    <!-- Page level plugin JavaScript-->
    <script src="{{asset ('vendor/backend_vedor/chart.js/Chart.min.js')}}"></script>
    <script src="{{asset ('vendor/backend_vedor/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{asset ('vendor/backend_vedor/datatables/dataTables.bootstrap4.js') }}"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{asset ('js/backend_js/sb-admin.min.js') }}"></script>
    <!-- Custom scripts for this page-->
    <script src="{{asset ('js/backend_js/sb-admin-datatables.min.js') }}"></script>
    <script src="{{asset ('js/backend_js/sb-admin-charts.min.js') }}"></script>
  </div>
</body>

</html>
