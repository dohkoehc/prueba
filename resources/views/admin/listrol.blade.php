@extends('layout.index')

@section('content')


  <div class="">
    <a href="{{route('rol.create')}}" class="btn btn-success">Nuevo</a>
  </div>
<section class="content">


  <div class="card">
       <div class="card-header">
            <h3 class="card-title">Listado Roles</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        <!-- /.card-body -->
            <table  width="95%" cellpadding="0" cellspacing="0" border="0"  class="cell-border"   id="roles">
                <thead>
                       <tr>
                         <th>Id</th>
                         <th>Rol</th>
                         <th>Level</th>

                       </tr>
                </thead>
                <tbody>
                   @foreach($Rols as $Rol)
                       <tr>
                       <td>{{$Rol->id}}</td>
                       <td>{{$Rol->name}}</td>
                       <td>{{$Rol->level}}</td>
                       <td>
                         <a href="{{ route('rol.edit',$Rol->id) }}" class="fas fa-pen-fancy"></a>
                         {{-- <a href="{{ route('servclist.indexdet',$Rol->id) }}" class="fas fa-pen-fancy"></a> --}}
                         <a href="" class="fas fa-ban"></a>
                       {{-- <button class="ion ion-md-create" type="submit">Editar</button> --}}
                       </td>
                       </tr>
                   @endforeach
                </tbody>
            </table>

          </div>
    </div>
      <!-- /.card -->
    </section>
@stop
