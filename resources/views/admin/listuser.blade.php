@extends('layout.index')

@section('content')


  <div class="">
    <a href="{{route('C_User.create')}}" class="btn btn-success">Nuevo</a>
  </div>
<section class="content">


  <div class="card">
       <div class="card-header">
            <h3 class="card-title">Listado Usuarios</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        <!-- /.card-body -->
            <table  width="95%" cellpadding="0" cellspacing="0" border="0"  class="cell-border"   id="usuarios">
                <thead>
                       <tr>
                         <th>Id</th>
                         <th>username</th>
                         <th>rol_id</th>
                         <th>status</th>
                         <th>Acción</th>
                       </tr>
                </thead>
                <tbody>
                   @foreach($Users as $detUsers)
                       <tr>
                       <td>{{$detUsers->id}}</td>
                       <td>{{$detUsers->username}}</td>
                       <td>{{$detUsers->rol_id}}</td>
                       <td>{{$detUsers->status}}</td>
                       <td>
                         <a href="{{ route('C_User.edit',$detUsers->id) }}" class="fas fa-pen-fancy"></a>
                         <a href="" class="fas fa-ban"></a>
                       {{-- <button class="ion ion-md-create" type="submit">Editar</button> --}}
                       </td>
                       </tr>
                   @endforeach
                </tbody>
            </table>

          </div>
    </div>
      <!-- /.card -->
    </section>
@stop
