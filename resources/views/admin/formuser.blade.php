@extends('layout.index')
@section('content')


<section class="content">
        <div class="card">
              <div class="card-header">
                <div class="">
                  <h3 class="card-title">Formulario de Usuarios</h3>
                </div>
              </div>
  <!-- /.card-header -->
    <!-- /.card-body -->
              <div class="card-body">


                  <form class="form-horizontal" method="POST"   @if($DUser <> null) action="{{ route('C_User.update',$DUser[0]->id) }}"  @else action="{{ route('C_User.store') }}" @endif>
                    @csrf

                     @if($DUser <> null) @method('PUT')" @endif


                    <input type="hidden" id="id" name="id" class="form-control is-warning" placeholder="..." value="@if($DUser<> null){{$DUser[0]->id}}@endif">

                      <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group">
                             <label>username</label>
                             <input type="text" id="name" name="name" class="form-control is-warning" placeholder="..." value="@if($DUser<> null){{$DUser[0]->username}}@endif">
                           </div>
                         </div>
                         <div class="col-sm-6">
                           <div class="form-group">
                             <label>status</label>
                             <input type="number" id="stat" name="stat" class="form-control is-warning" placeholder=" ..." value="@if($DUser<> null){{$DUser[0]->status}}@endif">
                           </div>
                         </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group">
                             <label>Rol</label>
                             <select class="form-control" id="Idrol" name="Idrol">
                               @foreach($Drol as $rol)
                               <option value="{{$rol->id}}" >{{$rol->name}}</option>
                               @endforeach
                             </select>

                             </div>
                         </div>
                         <div class="col-sm-6">
                           <div class="form-group">
                             <label>Password</label>
                             <input type="password" id="pass" name="pass" class="form-control is-warning" placeholder=" ..." value="@if($DUser<> null){{$DUser[0]->password}}@endif">
                           </div>
                         </div>
                      </div>



                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                          <button type="submit" class="btn btn-block btn-outline-primary" @if($DUser<> null) hidden @endif>Guardar</button>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <button type="submit" class="btn btn-block btn-outline-secondary" @if($DUser == null) hidden @endif>Editar</button>
                          </div>
                        </div>
                      </div>
                  </form>


              </div>
          </div>
      <!-- /.card -->
    </section>

@endsection
