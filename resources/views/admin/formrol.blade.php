@extends('layout.index')
@section('content')


<section class="content">
        <div class="card">
              <div class="card-header">
                <div class="">
                  <h3 class="card-title">Formulario de Tipo Servicios</h3>
                </div>
              </div>
  <!-- /.card-header -->
    <!-- /.card-body -->
              <div class="card-body">


                  <form class="form-horizontal" method="POST"   @if($Drol <> null) action="{{ route('rol.update',$Drol[0]->id) }}"  @else action="{{ route('rol.store') }}" @endif>
                    @csrf

                     @if($Drol <> null) @method('PUT') @endif


                    <input type="hidden" id="cod" name="cod" class="form-control is-warning" placeholder="..." value="@if($Drol<> null){{$Drol[0]->id}}@endif">

                      <div class="row">
                        <div class="col-sm-6">
                           <div class="form-group">
                             <label>Name</label>
                             <input type="text" id="Na" name="Na" class="form-control is-warning" placeholder="..." value="@if($Drol<> null){{$Drol[0]->name}}@endif">
                           </div>
                         </div>
                         <div class="col-sm-6">
                           <div class="form-group">
                             <label>level</label>
                             <input type="text" id="le" name="le" class="form-control is-warning" placeholder=" ..." value="@if($Drol<> null){{$Drol[0]->level}}@endif">
                           </div>
                         </div>
                      </div>

                      <div class="row">
                        <div class="col-sm-6">
                          <div class="form-group">
                          <button type="submit" class="btn btn-block btn-outline-primary" @if($Drol<> null) hidden @endif>Guardar</button>
                          </div>
                        </div>
                        <div class="col-sm-6">
                          <div class="form-group">
                            <button type="submit" class="btn btn-block btn-outline-secondary" @if($Drol == null) hidden @endif>Editar</button>
                          </div>
                        </div>
                      </div>
                  </form>


              </div>
          </div>
      <!-- /.card -->
    </section>

@endsection
